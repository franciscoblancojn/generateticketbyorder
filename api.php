<?php
class GTPD_api{
    private $url = "https://us-central1-hikikaeru-d3002.cloudfunctions.net/api/passes/membershipObject/";
    private $apiKey = "";
    private $companyID = "";
    public function __construct()
    {
        $this->apiKey = get_option( 'GTPD_apiKey' );
        $this->companyID = get_option( 'GTPD_companyID' );
    }
    public function request($json , $url = null, $method = "POST")
    {
        if($url == null){
            $url = $this->url;
        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => $method,
        CURLOPT_POSTFIELDS => $json,
        CURLOPT_HTTPHEADER => array(
            'Authorization: APIKey '.$this->apiKey,
            'Content-Type: application/json'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;

    }
    public function getTicket($order_id)
    {
        //cedula
        $cedula = get_post_meta( $order_id, '_billing_cedula', true );

        $r = $this->request("",$this->url.$this->companyID.".".$cedula,"GET");
        $r = json_decode($r,true);
        if($r["isError"]=="true"){
            for ($i=0; $i < count($r['errorInfo']); $i++) { 
                if($r['errorInfo'][$i]["code"]=="validation/id_not_registered"){
                    $r = $this->generateTicket($order_id);
                    // $r = $this->request("",$this->url.$this->companyID.".".$cedula,"GET");
                    // $r = json_decode($r,true);
                }
            }
            return flase;
        }
        $r = json_encode($r);
        update_post_meta($order_id,'get',$r);
        return true;
    }
    public function generateTicket($order_id)
    {
        $order = wc_get_order( $order_id );
        //product ID
        $product_id = null;
        foreach ( $order->get_items() as $item_id => $item ) {
            $product_id = $item->get_product_id();
        }

        //name
        $name = $order->get_billing_first_name()." ".$order->get_billing_last_name();

        //cedula
        $cedula = get_post_meta( $order_id, '_billing_cedula', true );

        //periodo
        $periodo = get_post_meta( $product_id, 'periodo', true );
        $nMonthsAdd = 1;
        switch ($periodo) {
            case 'annuity':
                $nMonthsAdd = 12;
                break;
            case 'semester':
                $nMonthsAdd = 6;
                break;
            case 'trimester':
                $nMonthsAdd = 3;
                break;
            case 'monthly':
                $nMonthsAdd = 1;
                break;
        }

        //date
        $toDay = date_create(date("c"));
        $newDate = date_create(date("c"));
        date_add($newDate, date_interval_create_from_date_string($nMonthsAdd." months"));

        //json
        $json = array(
            "id"                => $this->companyID.".".$cedula ,
            "extraData"         => array(
                "key1"          => "aValue",
                "key2"          => 2,
                "key3"          => true,
                "key4"          => array(
                    "innerKey1" => array(
                        "innerInnerKey1"    => 1
                    ),
                    "innerKey2" => [1,2,3]
                )
            ),
            "membershipTypeId"  => $this->companyID.".".$periodo,
            "state"             => "active", 
            "nfcValue"          => $cedula, 
            "ownerName"         => $name, 
            "barcode"           => array(
                "alternateText" => $cedula, 
                "type"          => "qrCode", 
                "value"         => $this->companyID.".".$cedula 
            ),
            "validityDates"     => array(
                "start"         => date_format($toDay,"c"), 
                "startOffset"   => -18000, 
                "end"           => date_format($newDate,"c"), 
                "endOffset"     => -18000
            )
        );
        $json = json_encode($json);
        update_post_meta($order_id,'json',$json);
        $r = $this->request($json);
        update_post_meta($order_id,'r',$r);
        $r = json_decode($r,true);
        if ($r['isError'] != "true") {
            update_post_meta($order_id,'uploadedId',$r['data'][0]['id']);
        }
        return $json;
    }
    public function fijarUrlRespuesta()
    {
        $json = array(
            "url"   => plugin_dir_url( __FILE__ )."action.php",
            "kind"  => "pkpassUploaded"
        );
        $json = json_encode($json);
        $r = $this->request($json , "https://us-central1-hikikaeru-d3002.cloudfunctions.net/api/webhooks");
    }
}