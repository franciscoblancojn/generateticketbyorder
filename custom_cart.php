<?php
function GTPD_limitarNumeroDeProductos ( $passed, $product_id )
{
	$periodo = get_post_meta($product_id,'periodo',true);
	if($periodo != null && $periodo != ""){
		wc_empty_cart();
	}else{
		global $woocommerce;
		$items = $woocommerce->cart->get_cart();

		foreach($items as $item => $values) { 
			$product_id2 = $values['data']->get_id();
			$periodo = get_post_meta($product_id2,'periodo',true);
			if($periodo != null && $periodo != ""){
				wc_empty_cart();
				break;
			}
		} 
	}

    return $passed;
}

add_filter( 'woocommerce_add_to_cart_validation', 'GTPD_limitarNumeroDeProductos',10,3  );
