<?php
add_action( 'admin_menu', 'GTPD_option_page_add_menu' );
function GTPD_option_page_add_menu() {
	add_menu_page(
		'GTPD_option', // page <title>Title</title>
		'Admin Ticket', // menu link text
		'manage_options', // capability to access the page
		'GTPD_option-slug', // page URL slug
		'GTPD_function_option_page', // callback function /w content
		'dashicons-tickets-alt', // menu icon
		5 // priority
	);
 
}
add_action( 'admin_init',  'GTPD_option_page_register_setting' );
 
function GTPD_option_page_register_setting(){
 
	register_setting(
		'GTPD_option_settings', // settings group name
		'GTPD_apiKey', // option name
		'sanitize_text_field' // sanitization function
	);
    register_setting(
		'GTPD_option_settings', // settings group name
		'GTPD_companyID', // option name
		'sanitize_text_field' // sanitization function
	);
    register_setting(
		'GTPD_option_settings', // settings group name
		'GTPD_email', // option name
		'sanitize_text_field' // sanitization function
	);
 
	add_settings_section(
		'GTPD_option_settings_section_id', // section ID
		'', // title (if needed)
		'', // callback function (if needed)
		'GTPD_option-slug' // page slug
	);
 
	add_settings_field(
		'GTPD_apiKey',
		'Api Key',
		'GTPD_apiKey_function', // function which prints the field
		'GTPD_option-slug', // page slug
		'GTPD_option_settings_section_id', // section ID
    );
	add_settings_field(
		'GTPD_companyID',
		'Company ID',
		'GTPD_companyID_function', // function which prints the field
		'GTPD_option-slug', // page slug
		'GTPD_option_settings_section_id', // section ID
    );
	add_settings_field(
		'GTPD_email',
		'Email',
		'GTPD_email_function', // function which prints the field
		'GTPD_option-slug', // page slug
		'GTPD_option_settings_section_id', // section ID
    );
}
function GTPD_apiKey_function(){
	$text = get_option( 'GTPD_apiKey' );
	printf(
		'<input type="text" id="GTPD_apiKey" name="GTPD_apiKey" value="%s" />',
		esc_attr( $text )
	);
}
function GTPD_companyID_function(){
	$text = get_option( 'GTPD_companyID' );
	printf(
		'<input type="text" id="GTPD_companyID" name="GTPD_companyID" value="%s" />',
		esc_attr( $text )
	);
}
function GTPD_email_function(){
	$text = get_option( 'GTPD_email' );
	printf(
		'<input type="email" id="GTPD_email" name="GTPD_email" value="%s" />',
		esc_attr( $text )
	);
}
function GTPD_function_option_page(){
    ?> 
    <form method="post" action="options.php">
        <h1>
            Administrador de Ticket
        </h1>
        <p>
            Por favor complete los datos para que el proceso de generar Ticket funcione correctamente
        </p>
        <?php
            settings_fields( 'GTPD_option_settings' ); 
            do_settings_sections( 'GTPD_option-slug' ); 
			submit_button();
			
			$apyKey = get_option( 'GTPD_apiKey' );
			if( $apyKey!="" && isset($apyKey)){
				$api = new GTPD_api();
				$api->fijarUrlRespuesta();
			}
        ?>
    </form>
    <?php
}