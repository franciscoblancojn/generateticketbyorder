<?php
function GTPD_add_cedula_checkout($checkout_fields)
{
    $checkout_fields['billing']['billing_cedula'] = array(
        'label'     => __('Cedula', 'woocommerce'),
        'required'  => true,
    );
    return $checkout_fields;
}
add_filter('woocommerce_checkout_fields', 'GTPD_add_cedula_checkout');
//show
add_action( 'woocommerce_admin_order_data_after_billing_address', 'GTPD_show_cedula_order', 10, 1 );
function GTPD_show_cedula_order( $order ) {    
   $order_id = $order->get_id();
   if ( get_post_meta( $order_id, '_billing_cedula', true ) ) 
   echo '<p><strong>Cedula:</strong> ' . get_post_meta( $order_id, '_billing_cedula', true ) . '</p>';
}