<?php
require_once(preg_replace('/wp-content.*$/','',__DIR__).'wp-load.php');

$data = json_decode(file_get_contents('php://input'), true);
if(isset($data)){
    $_POST = $data;
}

$uploadedId = $_POST['uploadedId'];

$order_ids = wc_get_orders(array(
    'meta_key'     => 'uploadedId',
    'meta_value'   => $uploadedId,
    'meta_compare' => '==',
    'return'       => 'ids',
));

$api = new GTPD_api();
for ($i=0; $i < count($order_ids); $i++) { 
    $order_id = $order_ids[$i];

    $r = $api->getTicket($order_id);
    if($r == true){
        $get = get_post_meta($order_id,'get',true);
        $get = json_decode($get,true);
        if(isset($get['data'][0]['downloadUrls'])){
            GTPD_sendTicketEmail($order_id);
        }
    }
}
