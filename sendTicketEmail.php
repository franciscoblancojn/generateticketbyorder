<?php
function GTPD_sendTicketEmail($order_id)
{
    $order = wc_get_order( $order_id );
    $get = get_post_meta($order_id,'get',true);
    $get = json_decode($get,true);
    
    $from = parse_url(get_site_url())["host"].' <'.get_option( 'GTPD_email' ) .'>';
    $subject = 'Suscripcion '.get_bloginfo();
    $to = $order->get_billing_email();
    
    // To send HTML mail, the Content-type header must be set
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    
    $headers .= 'From: '.$from."\r\n".
            'Reply-To: '.$from."\r\n" .
            'X-Mailer: PHP/' . phpversion();

    ob_start();
    ?>
    <html>
        <body>
            <h1><?=_("Hola")?></h1>
            <p>
                <?=_("Gracias por registarte en ")?>
                <a href="<?=get_site_url()?>">
                    <?=get_site_url()?>
                </a>
            </p>
            <h2>
                <?=_("Obten tu Ticket Aqui:")?>
            </h2>
            <h3>
                <a href="<?=$get['data'][0]['downloadUrls']['google']?>">
                    google
                </a>
            </h3>
            <h3>
                <a href="<?=$get['data'][0]['downloadUrls']['apple']?>">
                    apple
                </a>
            </h3>
        </body>
    </html>
    <?php
    $message = ob_get_clean();
    
    // Sending email
    if(wp_mail($to, $subject, $message, $headers)){
        update_post_meta($order_id,"Email_Send","OK");
        echo 'Your mail has been sent successfully.';
    } else{
        echo 'Unable to send email. Please try again.';
        update_post_meta($order_id,"Email_Send","Error");
    }
}