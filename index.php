<?php
/*
Plugin Name: Generar Ticket por Order
Plugin URI: https://startscoinc.com/es/
Description: Genera un Ticket al procesar un pedido y lo envia por correo eleectronico al cliente
Author: Startsco
Version: 0.1
Author URI: https://startscoinc.com/es/#
License: 
*/

require_once plugin_dir_path( __FILE__ ) . 'sendTicketEmail.php';
require_once plugin_dir_path( __FILE__ ) . 'api.php';
require_once plugin_dir_path( __FILE__ ) . 'page_option.php';
require_once plugin_dir_path( __FILE__ ) . 'change_order.php';
require_once plugin_dir_path( __FILE__ ) . 'custom_checkout.php';
require_once plugin_dir_path( __FILE__ ) . 'custom_cart.php';

register_activation_hook( __FILE__, 'GTPD_plugin_activation' );